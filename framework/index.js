import { VikunjaInfo, VikunjaLogin, MailboxLayer } from './services';

const apiProvider = () => ({
  auth: () => new VikunjaLogin(),
  info: () => new VikunjaInfo(),
  mailBox: () => new MailboxLayer(),
});

export { apiProvider };
