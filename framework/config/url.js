const urls = {
  reqres: 'https://reqres.in',
  vikunja: 'https://try.vikunja.io',
  airportGap: 'https://airportgap.dev-tester.com/api',
  kaiten: 'https://perchishka.kaiten.io/api/v1',
  mailboxlayer: 'https://apilayer.net/api',
};
export { urls };
