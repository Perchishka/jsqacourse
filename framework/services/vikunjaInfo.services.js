import fetch from 'node-fetch';
import supertest from 'supertest';

import { urls } from '../config';

const VikunjaInfo = function VikunjaInfo() {
  this.get = async function getVikunjaInfo(token) {
    const r = await supertest(urls.vikunja)
      .get('/api/v1/info')
      .set('Accept', 'application/json')
      .set('Authorization', `Bearer ${token}`);
    return r;
  };
  this.getInfo = async function getInfoVikunja(token) {
    const r = await fetch(`${urls.vikunja}/api/v1/info`, {
      method: 'get',
      headers: { Authorization: `Bearer ${token}` },
    });
    return r;
  };
};

export { VikunjaInfo };
