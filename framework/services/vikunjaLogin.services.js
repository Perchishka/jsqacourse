import fetch from 'node-fetch';
import supertest from 'supertest';
import { decorateService } from '../../lib/decorate';

import { urls } from '../config';
// todo Переименовать в Авторизацию
const VikunjaLogin = function vikunjaLogin() {
  this.post = async function postVikunjaLogin(user) {
    const r = await fetch(`${urls.vikunja}/api/v1/login`, {
      method: 'post',
      body: JSON.stringify(user),
      headers: { 'Content-Type': 'application/json' },
    });
    return r;
  };
  this.authorize = async function postVikunjaLogin(user) {
    const r = await supertest(`${urls.vikunja}`).post('/api/v1/login').set('Accept', 'application/json').send(user);
    return r;
  };
  this.register = async function postVikunjaLogin(user) {
    const r = await supertest(`${urls.vikunja}`).post('/api/v1/register').set('Accept', 'application/json').send(user);
    return r;
  };
  decorateService(this);
};
export { VikunjaLogin };
