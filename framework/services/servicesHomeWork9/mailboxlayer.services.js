import supertest from 'supertest';
import { urls } from '../../config';

const MailboxLayer = function MailboxLayer() {
  this.check = async function check(accessKey, email, smtp) {
    const r = await supertest(`${urls.mailboxlayer}`)
      .get(`/check?access_key=${accessKey}&email=${email}&smtp=${smtp}&format=1`).set('Accept', 'application/json');
    return r;
  };
};
export { MailboxLayer };
