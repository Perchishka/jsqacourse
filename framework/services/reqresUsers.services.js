import fetch from 'node-fetch';
import supertest from 'supertest';

import { urls } from '../config';

const ReqresUsers = function ReqresUsers() {
  this.get = async function getReqres() {
    const r = await supertest(urls.reqres).get('/api/users?page=2').set('Accept', 'application/json');
    return r;
  };
  this.getUsersList = async function getReqres() {
    const r = await fetch(`${urls.reqres}/api/users?page=2`);
    return r;
  };
};

export { ReqresUsers };
