function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

const RandomName = function RandomName(name) {
  return `${name}${getRandomInt(999)}`;
};

export { RandomName };
