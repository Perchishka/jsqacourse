import { expect, test } from '@jest/globals';
import { apiProvider } from '../framework';

describe('Отправляем http', () => {
  const accesskey = '6075ebb0ec96f6b7b1e0f44f155817e5';
  const inputEmail = 'pertseva_ekaterina@msn.com';
  test('Validate email address msn.com smtp_check=1', async () => {
    const { body } = await apiProvider().mailBox().check(accesskey, inputEmail, 1);
    expect(body.smtp_check).toBe(true);
    expect(body.free).toBe(true);
    expect(body.email).toEqual(inputEmail);
    expect(body.domain).toEqual('msn.com');
    expect(body.user).toEqual('pertseva_ekaterina');
    expect(body.disposable).toBe(false);
  });
  test('Validate email address mail.ru smtp_check=0', async () => {
    const { body } = await apiProvider()
      .mailBox().check(accesskey, inputEmail, 0);
    expect(body.smtp_check).toBe(null);
    expect(body.free).toBe(true);
    expect(body.email).toEqual(inputEmail);
    expect(body.domain).toEqual('msn.com');
    expect(body.user).toEqual('pertseva_ekaterina');
    expect(body.disposable).toBe(false);
  });
  test.each([
    ['', 'ek@mail.hu', 101, 'missing_access_key'],
    [accesskey, '', 210, 'no_email_address_supplied'],
    ['123', '123', 101, 'invalid_access_key'],
  ])('Negative test cases', async (key, mail, errorCode, type) => {
    const { body } = await apiProvider().mailBox().check(key, mail);
    expect(body.success).toBe(false);
    expect(body.error.code).toEqual(errorCode);
    expect(body.error.type).toEqual(type);
  });
});
